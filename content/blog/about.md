+++
title = "hi, i'm jane."
date = 2023-04-13
+++

i'm a pretty private person, for the most part. i like programming, sometimes. i like drawing, most of the time. i want to learn how to make music at some point. i'm kind of obsessed with my little pony, and also homestuck to a lesser extent. 

i dislike when people approach me, online or in person, with no prior warning. please don't expect me to respond to messages.